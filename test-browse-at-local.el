(describe "clean-uri"
  (describe "use-url-protocol"
    (it "removes \"git@\" from start"
      (expect (browse-at-local--use-url-protocol
               "git@gitlab.com:lafrenierejm/browse-at-local.git")
              :to-equal "ssh://gitlab.com/lafrenierejm/browse-at-local.git")))
  (describe "strip-git-extension"
    (it "removes \".git\" from the end"
      (expect (browse-at-local--strip-git-extension
               "git@gitlab.com:lafrenierejm/browse-at-local.git")
              :to-equal "git@gitlab.com:lafrenierejm/browse-at-local")))
  (it "removes \"git@\" from start and \".git\" from end"
    (expect (browse-at-local--clean-uri
             "git@gitlab.com:lafrenierejm/browse-at-local.git")
            :to-equal "ssh://gitlab.com/lafrenierejm/browse-at-local")))

(describe "browse-at-local--gitlab-parse"
  (it "parses Git URI"
    (expect (browse-at-local--gitlab-parse
             "git@gitlab.com:lafrenierejm/browse-at-local.git")
            :to-equal (make-browse-at-local-urlobj
                       :hostname "gitlab.com"
                       :project "lafrenierejm"
                       :repository "browse-at-local")))
  (it "parses homepage"
    (expect (browse-at-local--gitlab-parse
             "https://gitlab.com/lafrenierejm/browse-at-local")
            :to-equal (make-browse-at-local-urlobj
                       :hostname "gitlab.com"
                       :project "lafrenierejm"
                       :repository "browse-at-local")))
  (it "parses branch"
    (expect (browse-at-local--gitlab-parse
             "https://gitlab.com/lafrenierejm/browse-at-local/blob/feature/parse-gitlab")
            :to-equal (make-browse-at-local-urlobj
                       :hostname "gitlab.com"
                       :project "lafrenierejm"
                       :repository "browse-at-local"
                       :branch-and-path '("feature" "parse-gitlab"))))
  (it "parses root file"
    (expect (browse-at-local--gitlab-parse
             "https://gitlab.com/lafrenierejm/browse-at-local/blob/feature/parse-gitlab/browse-at-local.org")
            :to-equal (make-browse-at-local-urlobj
                       :hostname "gitlab.com"
                       :project "lafrenierejm"
                       :repository "browse-at-local"
                       :branch-and-path '("feature" "parse-gitlab" "browse-at-local.org")))
    (expect (browse-at-local--gitlab-parse
             "https://gitlab.com/ambrevar/emacs-disk-usage/blob/master/COPYING")
            :to-equal (make-browse-at-local-urlobj
                       :hostname "gitlab.com"
                       :project "ambrevar"
                       :repository "emacs-disk-usage"
                       :branch-and-path '("master" "COPYING"))))
  (it "parses URLs targeting a line number"
    (expect (browse-at-local--gitlab-parse
             "https://gitlab.com/lafrenierejm/browse-at-local/blob/feature/parse-gitlab/browse-at-local.org#L6")
            :to-equal (make-browse-at-local-urlobj
                       :hostname "gitlab.com"
                       :project "lafrenierejm"
                       :repository "browse-at-local"
                       :branch-and-path '("feature" "parse-gitlab" "browse-at-local.org")
                       :start 6))))

(describe "browse-at-local--gitlab-clean-path"
  (it "cleans paths ending with a prefix"
    (expect (browse-at-local--gitlab-clean-path "lafrenierejm/browse-at-local/blob")
            :to-equal '("lafrenierejm" "browse-at-local")))
  (it "ignores non-prefixed paths"
    (expect (browse-at-local--gitlab-lean-path "lafrenierejm/browse-at-local")
            :to-equal '("lafrenierejm" "browse-at-local")))
  (it "cleans paths containing a prefix"
    (expect (browse-at-local--gitlab-clean-path
             "lafrenierejm/browse-at-local/blob/feature/parse-gitlab/browse-at-local.org")
            :to-equal '("lafrenierejm"
                        "browse-at-local"
                        "feature/parse-gitlab"
                        "browse-at-local.org"))))

(describe "browse-at-local--gitlab-parse-path"
  (it "parses paths that end at the project"
    (expect (browse-at-local--gitlab-parse-path "lafrenierejm/browse-at-local")
            :to-equal (make-browse-at-local--gitlab-path
                       :project "lafrenierejm"
                       :repository "browse-at-local")))
  (it "parses paths that end with a prefix"
    (expect (browse-at-local--gitlab-parse-path "lafrenierejm/browse-at-local/blob")
            :to-equal (make-browse-at-local--gitlab-path
                       :project "lafrenierejm"
                       :repository "browse-at-local")))
  (it "parses paths that end with the branch"
    (expect (browse-at-local--gitlab-parse-path
             "lafrenierejm/browse-at-local/blob/feature/parse-gitlab")
            :to-equal (make-browse-at-local--gitlab-path
                       :project "lafrenierejm"
                       :repository "browse-at-local"
                       :branch "feature/parse-gitlab")))
  (it "parses paths containing a file path"
    (expect (browse-at-local--gitlab-parse-path
             "lafrenierejm/browse-at-local/blob/feature/parse-gitlab/browse-at-local.org")
            :to-equal (make-browse-at-local--gitlab-path
                       :project "lafrenierejm"
                       :repository "browse-at-local"
                       :branch "feature/parse-gitlab"
                       :filepath "browse-at-local.org"))
    (expect (browse-at-local--gitlab-parse-path
             "lafrenierejm/browse-at-local/tree/master")
            :to-equal (make-browse-at-local--gitlab-path
                       :project "lafrenierejm"
                       :repository "browse-at-local"
                       :branch "master"))))

(describe "browse-at-local--gitlab-parse-query"
  (it "extracts a single line number"
    (expect (browse-at-local--gitlab-parse-target "L6")
            :to-equal (make-browse-at-local--lines
                       :start 6))))

(describe "browse-at-local--build-origin-uri"
  (it "returns Git URI for matching repositories"
    (expect (let ((browse-at-local-push-regexp (rx (one-or-more anything))))
              (browse-at-local--build-origin-uri
               :hostname "hostname"
               :project "project"
               :repository "repository"))
            :to-equal "git@hostname:project/repository.git"))
  (it "returns HTTPS URI for non-matching repositories"
    (expect (let ((browse-at-local-push-regexp "nonexistent"))
              (browse-at-local--build-origin-uri
               :hostname "hostname"
               :project "project"
               :repository "repository"))
            :to-equal "https://hostname/project/repository.git")))

(describe "browse-at-local--hostname-to-dns"
  (it "reverses a hostname"
    (expect (browse-at-local--hostname-to-dns "gitlab.com")
            :to-equal "com.gitlab")))

(describe "browse-at-local-build-destination"
  (it "results in absolute paths"
    (expect (let ((browse-at-local-source-directory "/home/user/source"))
              (browse-at-local-build-destination
               :dns "com.gitlab"
               :project "lafrenierejm"
               :repository "browse-at-local"))
            :to-equal "/home/user/source/com.gitlab/lafrenierejm/browse-at-local")))

(describe "browse-at-local-local"
  (it "uses branch for commit"
    (expect (let ((browse-at-local-push-regexp (rx (one-or-more anything)))
                  (browse-at-local-source-directory "/home/user/source"))
              (browse-at-local-local (make-browse-at-local-urlobj
                                      :hostname "gitlab.com"
                                      :project "lafrenierejm"
                                      :repository "browse-at-local"
                                      :filepath "browse-at-local.org"
                                      :branch "feature/parse-gitlab"
                                      :start 6)))
            :to-equal (browse-at-local-local--create
                       :remote-uri "git@gitlab.com:lafrenierejm/browse-at-local.git"
                       :commit "feature/parse-gitlab"
                       :root "/home/user/source/com.gitlab/lafrenierejm/browse-at-local"
                       :filepath "browse-at-local.org"
                       :commit "feature/parse-gitlab"
                       :start 6
                       :end nil))))
