#+TITLE: Browse at Local
#+AUTHOR: Joseph M LaFreniere (lafrenierejm)
#+EMAIL: joseph@lafreniere.xyz
#+PROPERTY: header-args+ :noweb yes

#+BEGIN_SRC emacs-lisp :tangle yes
;;; browse-at-local.el --- Open a buffer corresponding to a Git forge URL -*- lexical-binding: t; -*-

;; Copyright (C) 2019 Joseph LaFreniere

;; Author: Joseph LaFreniere <joseph@lafreniere.xyz>
;; Version 0.0.1
;; Keywords: convenience, git, vc
;; Package-Requires: ()

;; This file is not part of GNU emacs.

;; <<license>>

;;; Commentary:
;; <<commentary>>

;;; Code:

<<dependencies>>

<<group-definition>>

;;; Entrypoints:
<<entrypoints>>

;;; Clean and Parse URIs:
<<clean-and-parse-uris>>

;;; Open Locally:
<<open-locally>>

(provide 'browse-at-local)
;;; browse-at-local.el ends here
#+END_SRC

* License
:PROPERTIES:
:HEADER-ARGS+: :noweb-ref license
:END:

** Code
All source code in this file is licensed under the [[http://www.gnu.org/licenses/gpl-3.0.html][GNU Public License v3]] or later:
#+BEGIN_SRC text :noweb-ref license
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with GNU Emacs.
If not, see <https://www.gnu.org/licenses/>.
#+END_SRC

** Prose
All prose in this file is licensed under the [[http://www.gnu.org/licenses/fdl-1.3.html][GNU Free Documentation License (FDL) v1.3]]:
#+BEGIN_SRC text
Copyright 2019 Joseph LaFreniere.
Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3 or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
#+END_SRC

* Commentary
:PROPERTIES:
:HEADER-ARGS+: :noweb-ref commentary
:END:

#+BEGIN_SRC text

#+END_SRC

* Code

** Dependencies
:PROPERTIES:
:HEADER-ARGS+: :noweb-ref dependencies
:END:

The following packages are required.
#+BEGIN_SRC emacs-lisp
(require 'rx)
(require 's)
(require 'url-parse)
#+END_SRC

** Group Definition
:PROPERTIES:
:HEADER-ARGS+: :noweb-ref group-definition
:END:

Define a group for this package.
#+begin_src emacs-lisp
(defgroup browse-at-local nil
  "Open Git forge URI locally."
  :prefix "browse-at-local-"
  :group 'applications)
#+end_src

Map hostnames to remote types.
#+begin_src emacs-lisp
(defcustom browse-at-local-hostname-mapping
  '(("github.com" . "github")
    ("gitlab.com" . "gitlab"))
  "Alist of hostnames to remote types."

  :type '(alist :key-type (string :tag "hostname")
                :value-type (choice
                             (const :tag "GitHub" "github")
                             (const :tag "GitLab" "gitlab")))
  :group 'browse-at-local)
#+end_src

** Entrypoints
:PROPERTIES:
:HEADER-ARGS+: :noweb-ref entrypoints
:END:

#+BEGIN_SRC emacs-lisp
(defun browse-at-local (uri)
  "Open Git repository URI locally."
  (interactive "MGit URI: ")
  (let ((urlobj (-> (browse-at-local--clean-uri uri)
                    (browse-at-local--gitlab-parse))))
    (let* ((hostname (browse-at-local-urlobj-hostname urlobj))
           (dns (browse-at-local--hostname-to-dns
                 (browse-at-local-urlobj-hostname urlobj)))
           (project (browse-at-local-urlobj-project urlobj))
           (repository (browse-at-local-urlobj-repository urlobj))
           (root (browse-at-local-build-destination
                  :dns dns
                  :project project
                  :repository repository))
           (git-dir (magit-convert-filename-for-git root))
           (remote-uri (browse-at-local--build-origin-uri
                        :hostname hostname
                        :project project
                        :repository repository))
           (branch-and-path
            (browse-at-local-urlobj-branch-and-path urlobj))
           (remote-name
            ;; clone or fetch the repo
            (browse-at-local--create-local-copy remote-uri project root))
           (branch
            (or (browse-at-local--branch-name
                 branch-and-path remote-name git-dir)
                "master"))
           (path (s-chop-prefix (concat branch "/") branch-and-path)))
      ;; stash any current changes then checkout the commit
      (magit-status root)
      (magit-git-success
       "-C" git-dir
       "push"
       "--message" (format "browse-at-local for %s" uri))
      (magit-git-success "-C" git-dir "checkout" branch)
      ;; open the path, if any
      (when (and path (not (string-empty-p path)))
        (find-file (expand-file-name path))))))
#+END_SRC

** Clean and Parse URIs
:PROPERTIES:
:HEADER-ARGS+: :noweb-ref clean-and-parse-uris
:END:

*** Cleaning

Every URI needs to be in a canonical URL form before being passing to forge-specific parsers.
Define a function to perform this function.
Each cleaning step is decoupled as much as possible from the others.
#+BEGIN_SRC emacs-lisp
(defun browse-at-local--clean-uri (uri)
  "Clean a URI before parsing."
  (-> uri
      (browse-at-local--use-url-protocol)
      (browse-at-local--strip-git-extension)))
#+END_SRC

**** Helper Functions
Define a function to use an actual URL protocol form.
#+BEGIN_SRC emacs-lisp
(defun browse-at-local--use-url-protocol (uri)
  "Convert a Git URI into a URL with the \"ssh\" protocol."
  (if (string-match (rx string-start "git@"
                        (group (minimal-match (one-or-more not-newline))) ":"
                        (group (one-or-more not-newline))
                        string-end)
                    uri)
      (concat "ssh://" (match-string 1 uri)
              "/" (match-string 2 uri))
    uri))
#+END_SRC

Define a function to remove ".git".
#+BEGIN_SRC emacs-lisp
(defun browse-at-local--strip-git-extension (uri)
  "Remove the \".git\" extension (if any) from the URI."
  (if (string-match (rx string-start
                        (group (one-or-more not-newline))
                        ".git"
                        string-end)
                    uri)
      (match-string 1 uri)
    uri))
#+END_SRC

*** Parsing

Every URI will be parsed into the following components, with nonexistent components being src_emacs-lisp{nil}:
- DNS name (omitting the leading period), which is the hostname reversed;
- project name;
- repository name;
- file path;
- branch name;
- commit name;
- starting line number;
- ending line number.


Define the class used to hold URLs' components.
It is not always possible to distinguish from a URL where a branch name ends and the file path begins.
That information can always be obtained after cloning the repository when the branch names for a given remote are known, but for now that information remains concatenated in a single slot.
#+BEGIN_SRC emacs-lisp
(cl-defstruct browse-at-local-urlobj
  hostname
  project
  repository
  (branch-and-path nil)
  (start nil)
  (end nil))
#+END_SRC

Define the structure to represent contiguous line numbers.
#+BEGIN_SRC emacs-lisp
(cl-defstruct browse-at-local--lines
  (start nil)
  (end nil))
#+END_SRC

*** GitLab
#+BEGIN_SRC emacs-lisp
(defun browse-at-local--gitlab-parse (uri)
  "Return a browse-at-local-urlobj of the URI's components."
  (let* ((url (browse-at-local--clean-uri uri))
         (urlobj (url-generic-parse-url url))
         (hostname (url-host urlobj))
         (path (browse-at-local--gitlab-parse-path (url-filename urlobj)))
         (target (browse-at-local--gitlab-parse-target (url-target urlobj))))
    (make-browse-at-local-urlobj
     :hostname hostname
     :project (browse-at-local--gitlab-path-project path)
     :repository (browse-at-local--gitlab-path-repository path)
     :branch-and-path (browse-at-local--gitlab-path-rest path)
     :start (browse-at-local--lines-start target)
     :end (browse-at-local--lines-end target))))
#+END_SRC

**** Path
Define the structure to hold a GitLab URL's path components.
#+BEGIN_SRC emacs-lisp
(cl-defstruct browse-at-local--gitlab-path
  project repository (rest nil))
#+END_SRC

Define a function to split a path and remove any prefixes.
#+BEGIN_SRC emacs-lisp
(defun browse-at-local--gitlab-clean-path (path)
  "Split a GitLab url PATH and remove any prefixes from it."
  (let ((path-split (s-split (rx "/") path t)))
    (if (seq-contains
         '("blob" "raw" "tree")
         (nth 2 path-split))
        (append (subseq path-split 0 2) (subseq path-split 3))
      path-split)))
#+END_SRC

Define the function to parse a URL's path.
#+BEGIN_SRC emacs-lisp
(defun browse-at-local--gitlab-parse-path (path)
  "Parse a GitLab url PATH."
  (let* ((path-split (browse-at-local--gitlab-clean-path path)))
    (make-browse-at-local--gitlab-path
     :project (nth 0 path-split)
     :repository (nth 1 path-split)
     :rest (s-join "/" (nthcdr 2 path-split)))))
#+END_SRC

**** Target
Define a function to parse the URL's target component.
#+BEGIN_SRC emacs-lisp
(defun browse-at-local--gitlab-parse-target (target)
  "Parse a GitLab url TARGET."
  (if target
      (let ((target-clean (s-replace-all '(("L" . "")) target)))
        (make-browse-at-local--lines :start (string-to-number target-clean)))
    (make-browse-at-local--lines)))
#+END_SRC

** Open Locally
:PROPERTIES:
:HEADER-ARGS+: :noweb-ref open-locally
:END:

*** Assemble Information for Opening Locally

**** Determine Clone Destination

The destination path for cloning a repository (or looking for an already-cloned repositories) is derived from the repository host's DNS name and the repository's project and name.
This is broken out into a separate function to make it easier to redefine.
#+BEGIN_SRC emacs-lisp
(cl-defun browse-at-local-build-destination (&key dns project repository)
  "Build the clone destination path for DNS, PROJECT, and REPOSITORY."
  (f-join (expand-file-name browse-at-local-source-directory)
          dns
          project
          repository))
#+END_SRC

Define the variable to hold the path to the user's source code.
#+BEGIN_SRC emacs-lisp
(defcustom browse-at-local-source-directory
  (case system-type
    ('gnu/linux
     (expand-file-name "~/source")))
  "The parent directory of all `browse-at-local' DNS-named directories.")
#+END_SRC

**** Determine URI for Cloning

The destination path for cloning a repository (or looking for an already-cloned repositories) is derived from the repository host's DNS name and the repository's project and name.
The default is to return a URI of the form =https://{hostname}/{project}/{repository}.git=.
However, if a combination of hostname, project, or repository matches one of the regular expressions in \src_emacs-lisp{browse-at-local-push-patterns}, a URI of the form =git@{hostname}:{project}/{repository}.git= will be returned instead.
#+BEGIN_SRC emacs-lisp
(cl-defun browse-at-local--build-origin-uri (&key hostname project repository)
  "Build a uri from HOSTNAME, PROJECT, and PROJECT for cloning and to set as \"origin\"."
  (if (string-match-p browse-at-local-push-regexp
                      (s-join "/" (list hostname project repository)))
      (format "git@%s:%s/%s.git" hostname project repository)
    (format "https://%s/%s/%s.git" hostname project repository)))
#+END_SRC

Define the regular expression matched against when determining push access to remotes.
#+BEGIN_SRC emacs-lisp
(defcustom browse-at-local-push-regexp
  (eval `(rx string-start (one-or-more not-newline)
             "/" ,user-login-name "/"
             (one-or-more not-newline) string-end))
  "Regular expression used to determine push access to a repo.

If a string derived from a repository matches this regular expression, it is assumed that the user has push access to that repository.
This regexp is intended to be compared against strings of the form \"{hostname}/{project}/{repository}\".")
#+END_SRC

Define the function to obtain a DNS name (minus the leading ".") from a fully qualified hostname.
#+BEGIN_SRC emacs-lisp
(defun browse-at-local--hostname-to-dns (hostname)
  "Convert fully-qualified HOSTNAME to DNS name without a leading \".\"."
  (s-join "." (nreverse (s-split (rx ".") hostname))))
#+END_SRC

*** Clone or Fetch the Repository

Now that the information from a URI has been cleaned and stored, a local representation of the repository can be created and opened.
1. The repository's root directory (henceforth =root=) shall be created if it doesn't already exist.
2. A local clone of repository shall be created in =root=
   1. If =root= already exists and is not empty, =root/.git= shall be looked for.
      1. If =root/.git= exists, it shall not be touched.
      2. If =root/.git= does /not/ exist, an error shall be raised stating that =root/= already exists but is not a Git repository.
   2. If =root= does /not/ already exist or is empty, the repository shall be cloned into that location.


#+BEGIN_SRC emacs-lisp
(defun browse-at-local--create-local-copy (remote-uri remote-name root)
  "Fetch REMOTE-URI (creating as REMOTE-NAME) in the repo at ROOT."
  (let ((git-dir (magit-convert-filename-for-git root)))
    (cond
     ;; if `root' does not exist...
     ((not (file-exists-p root))
      ;; create and populate the directory
      (browse-at-local--create-repo remote-uri remote-name root))
     ;; else if `root' exists and is not a directory...
     ((not (file-directory-p root))
      (error "%s exists but is not a directory" root))
     ;; else if `root' does not contain a `.git' subdirectory...
     ((not (magit-git-repo-p root t))
      (error "%s exists but is not a non-bare Git repository" root))
     ;; else `root' is an existing Git repository...
     (t
      ;; ensure this uri exists as a remote
      (or (browse-at-local--uri-present-p remote-uri git-dir)
          (browse-at-local--remote-add
           remote-uri remote-name git-dir))))))
#+END_SRC

**** New Clone

1. Create the directory.
2. Initialize a new Git repository in that directory.
3. Add and fetch the remote.


#+BEGIN_SRC emacs-lisp
(defun browse-at-local--create-repo (remote-uri remote-name root)
  "Clone the REMOTE-URI named REMOTE-NAME into directory ROOT."
  (make-directory root t)
  (magit-git-success
   "clone"
   "--origin" remote-name
   "--" remote-uri root)
  remote-name)
#+END_SRC

**** Existing Clone

Determine if a given URI already has already been assigned to a remote.
#+BEGIN_SRC emacs-lisp
(defun browse-at-local--uri-present-p (remote-uri git-dir)
  "Determine whether a REMOTE-URI exists for the local repo at GIT-DIR.

If found, fetch the rmeote and return its name.  Else return `nil'."
  (cl-loop
   for remote in (magit-git-lines "-C" git-dir "remote")
   if (string-equal remote-uri
                    (magit-git-string
                     "-C" git-dir
                     "remote" "get-url" remote))
   do (magit-git-success "-C" git-dir "fetch" remote)
   return remote))
#+END_SRC

Add and fetch a remote.
#+BEGIN_SRC emacs-lisp
(defun browse-at-local--remote-add (remote-uri remote-name git-dir)
  "Add and fetch REMOTE-URI named REMOTE-NAME to GIT-DIR, returning REMOTE-NAME."
  (unless (magit-git-success
           "-C" git-dir
           "remote" "add" "-f" remote-name remote-uri)
    (error "Failed to add remote \"%s %s\" to %s"
           remote-name remote-uri git-dir))
  remote-name)
#+END_SRC

*** Find the Branch Name

#+BEGIN_SRC emacs-lisp
(defun browse-at-local--branch-name (branch-and-path remote-name git-dir)
  "Extract the branch from BRANCH-AND-PATH for REMOTE-NAME in GIT-DIR."
  (let ((branch-longest ""))
    ;; iterate through branch names, recording the longest matching one
    (cl-loop
     for branch in (magit-git-lines
                    "-C" git-dir
                    "branch"
                    "--remotes"
                    "--list" (concat remote-name "*"))
     when (and (s-starts-with-p branch branch-and-path)
               (< (length branch-longest) (length branch)))
     do (setq branch-longest branch))
    ;; return branch-longest if it's not empty
    (and (not (string-empty-p branch-longest))
         branch-longest)))
#+END_SRC
